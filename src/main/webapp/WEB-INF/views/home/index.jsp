<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
 
pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Placovu Parser</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
 <script type="text/javascript">
 function parseFile()
	{
		var dataArray = {
				"fileType" : $("#fileType").val(),
				"filePath" : $("#filePath").val()
		}
		 $.ajax({
			 url: "<%=request.getContextPath().toString()%>/parsefile",
			 method: "post",
			 data: dataArray,
		 	 dataType : "json",
		 	 global : false,
			 success: function(response){
				 $("#messageDiv").text(response.message);
		 	 },
		 	 error: function(result){
		 	 }
		});
	}
</script>
  
</head>
<body>
	<div class="container">
		 <div class="panel panel-default">
	     	<div class="panel-heading"><h2>Placovu Parser</h2></div>
			 <div class="panel-body panel-primary" id="messageDiv">
			 
			 </div>
			 <div class="panel-body panel-primary" id="authenticateForm">
			    	<form class="form-horizontal">
					  <div class="form-group">
					    <label for="email">File Type</label>
					    <input type="text" class="form-control" id="fileType" name=""fileType"" >
					  </div>
					  <div class="form-group">
					    <label for="pwd">File Location</label>
					    <input type="text" class="form-control" id="filePath" name = "filePath">
					  </div>
					  <div class="form-group">
					    <input type="button" class="btn btn-primary"  value="submit" onclick="parseFile()">
					  </div>
					</form>
				</div>
	  	</div>
	</div>
</body>
</html>