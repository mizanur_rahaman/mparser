package com.placovuparser.dao;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import com.placovuparser.parsermanager.ExcelFileParserManager;

@Repository
public class FileParserDao {
	
	private static final Logger logger = Logger.getLogger(FileParserDao.class);

	private JdbcTemplate jdbcTemplate; 
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate)
	{
		this.jdbcTemplate = jdbcTemplate;
	}
	public boolean createDatabaseTable(String tableName, String columnString)
	{
		String createTableSql = "create table "+tableName + " ( " +columnString + " ) ";
        System.out.println(createTableSql);
		try
		{
			this.jdbcTemplate.execute(createTableSql);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			return false;
		}
		return true;
	}
	public void insertRow(String tableName,String columnString, String valuesString, ArrayList<Object> params){
		// TODO Auto-generated  stub
		try
		{
			String insertRowSql = "insert into "+tableName + " ( " +columnString + " ) values (" + valuesString + ")";
			logger.debug(insertRowSql);
			this.jdbcTemplate.update(insertRowSql,params.toArray());
		}
		catch(Exception ex)
		{
			logger.error(ex.getMessage());
			ex.printStackTrace();
		}
		
	}
	
	
}
