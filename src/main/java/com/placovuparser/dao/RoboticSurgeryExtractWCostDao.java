package com.placovuparser.dao;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.placovuparser.manager.connection.MongoDbConnectionManager;

@Repository
public class RoboticSurgeryExtractWCostDao {
	@Autowired MongoDbConnectionManager mongoDbConnectionManager;
	public void insertNewDocument(Document document, String tableName)
	{
		MongoDatabase database = mongoDbConnectionManager.getMongoDbConnection();
		MongoCollection<Document> collection = database.getCollection(tableName); 
		collection.insertOne(document);
	}
}
