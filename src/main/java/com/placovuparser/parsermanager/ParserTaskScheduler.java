package com.placovuparser.parsermanager;

import java.util.Date;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.placovuparser.service.TaskExecuterService;

@Service
public class ParserTaskScheduler {
	
	private static final Logger logger = Logger.getLogger(ParserTaskScheduler.class);
	private static boolean isRunCompleted = true;
    @Autowired TaskExecuterService taskExecuterService;
    
	public void run() {
		
		logger.info("Tast scheduler execute at : " + new Date());
		System.out.println("Tast scheduler execute at : " + new Date());
		if(isRunCompleted)
		{
			System.out.println("running");
			isRunCompleted = false;
			// download file if exist
			taskExecuterService.downloadFileFromRemoteServerUsingJSCH();
			// insert data in database
			taskExecuterService.InsertDataSqlServiceFromExcelFile();
			isRunCompleted = true;
		}
		
	}
}
