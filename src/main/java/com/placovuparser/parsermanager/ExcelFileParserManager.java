package com.placovuparser.parsermanager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.bson.Document;
import org.springframework.stereotype.Repository;

import com.placovuparser.manager.connection.JschRemoteConnection;
import com.placovuparser.model.FileData;
import com.placovuparser.util.Util;

@Repository
public class ExcelFileParserManager {
	
	private static final Logger logger = Logger.getLogger(ExcelFileParserManager.class);
	
	public ArrayList<Document> processFileForMongoDB(String filePath)
	{
		ArrayList<Document> dataList = new ArrayList<Document>();
		try 
		{
            FileInputStream excelFile = new FileInputStream(new File(filePath));
           // Workbook workbook = new XSSFWorkbook(excelFile);
            Workbook workbook = null;;
			try 
			{
				workbook = WorkbookFactory.create(new File(filePath));
			} 
			catch (EncryptedDocumentException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			catch (InvalidFormatException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(workbook == null)
				return null;
			
            Sheet datatypeSheet = workbook.getSheetAt(0);
            logger.info("sheet name: "+datatypeSheet.getSheetName());
            Iterator<Row> iterator = datatypeSheet.iterator();
            Map<Integer,String> columnMap = new HashMap<Integer,String>();
            int columnSize = 0;
            
            if(iterator.hasNext())
            {
            	
            	 Row currentRow = iterator.next();
                 Iterator<Cell> cellIterator = currentRow.iterator();
                 int noOfColumn = 0;
                 while (cellIterator.hasNext()) 
                 {
                     Cell currentCell = cellIterator.next();
                     if (currentCell.getCellTypeEnum() == CellType.STRING) 
                     {
                         String currentCellName = currentCell.getStringCellValue().replaceAll("[^a-zA-Z0-9]", "");
                         columnMap.put(noOfColumn++, currentCellName);
                     } 
                 }
                 logger.info("noOfColumn " +noOfColumn);
            }
            columnSize = columnMap.size();
            if(columnSize > 162)
            	columnSize = 162;
            
            while (iterator.hasNext())
            {
                Row currentRow = iterator.next();
                Document document = new Document();
                for(int cn=0; cn<columnSize; cn++)
                {
                    // If the cell is missing from the file, generate a blank one
                    // (Works by specifying a MissingCellPolicy)
                    Cell currentCell = currentRow.getCell(cn, Row.CREATE_NULL_AS_BLANK);
                    if (currentCell.getCellTypeEnum() == CellType.STRING)
                    {
                    	document.append(columnMap.get(cn), currentCell.getStringCellValue());
                    } 
                    else if (currentCell.getCellTypeEnum() == CellType.NUMERIC)
                    {
                    	DataFormatter fmt = new DataFormatter();
                    	String cellValue = fmt.formatCellValue(currentCell);
                     	if(Util.isParsableAsInt(cellValue))
                     		document.append(columnMap.get(cn), Integer.parseInt(cellValue));
                     	else if(Util.isParsableAsLong(cellValue))
                     		document.append(columnMap.get(cn), Long.parseLong(cellValue));
                     	else if(Util.isParsableAsDouble(cellValue))
                     		document.append(columnMap.get(cn), Double.parseDouble(cellValue));
                     	else
                     		document.append(columnMap.get(cn), cellValue);
                     	 
                    }
                    else if (currentCell.getCellTypeEnum() == CellType.BOOLEAN)
                    {
                    	boolean cellValue = currentCell.getBooleanCellValue();
                    	document.append(columnMap.get(cn), cellValue);
                    	
                    }
                    else if (currentCell.getCellTypeEnum() == CellType.FORMULA) 
                    {
                    	document.append(columnMap.get(cn), currentCell.getCellFormula());
                    }
                    else if (currentCell.getCellTypeEnum() == CellType.BLANK) 
                    {
                    	document.append(columnMap.get(cn), currentCell.getStringCellValue());
                    }
                    else if (currentCell.getCellTypeEnum() == CellType._NONE)
                    {
                    	document.append(columnMap.get(cn), null);
                    }
                    else
                    {
                    	document.append(columnMap.get(cn), null);
                    }
                }
                dataList.add(document);
           }
    
        } 
		catch (FileNotFoundException e) 
		{
            e.printStackTrace();
        }
		catch (IOException e) 
		{
            e.printStackTrace();
        }
		return dataList;
	}
	
	public FileData processFile(String filePath,JschRemoteConnection jschRemoteConnection) 
	{
     	FileData fileData = new FileData();
        Workbook workbook = null;;
		try 
		{
			try 
			{
				workbook = WorkbookFactory.create(new File(filePath));
			} catch (EncryptedDocumentException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvalidFormatException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(workbook == null)
				return null;
			
            Sheet datatypeSheet = workbook.getSheetAt(0);
            logger.info("sheet name: "+datatypeSheet.getSheetName());
            fileData.setDocumentName(datatypeSheet.getSheetName());
            
            Iterator<Row> iterator = datatypeSheet.iterator();
            Map<Integer,String> columnMap = new HashMap<Integer,String>();
            Map<Integer,String> typeMap = new HashMap<Integer,String>();
            Map<Integer,Integer> sizeMap = new HashMap<Integer,Integer>();
            ArrayList<Map<Integer,Object>> dataList = new ArrayList<Map<Integer,Object>>();
            
            if(iterator.hasNext())
            {
            	
            	 Row currentRow = iterator.next();
                 Iterator<Cell> cellIterator = currentRow.iterator();
                 int noOfColumn = 0;
                 while (cellIterator.hasNext()) 
                 {
                     Cell currentCell = cellIterator.next();
                     if (currentCell.getCellTypeEnum() == CellType.STRING) 
                     {
                         String currentCellName = currentCell.getStringCellValue().replaceAll("[^a-zA-Z0-9]", "");
                         columnMap.put(++noOfColumn, currentCellName);
                     } 
                 }
                 logger.info("noOfColumn " +noOfColumn);
            }
            
            int columnSize = columnMap.size();
            if(columnSize > jschRemoteConnection.getTotalField())
            	columnSize = jschRemoteConnection.getTotalField();
            while (iterator.hasNext()) 
            {
                Row currentRow = iterator.next();
                Iterator<Cell> cellIterator = currentRow.iterator();
                int noOfValue = 0;
                Map<Integer,Object> dataMap = new HashMap<Integer,Object>();
                for(int cn=0; cn<columnSize; cn++) {
                    Cell currentCell = currentRow.getCell(cn, Row.CREATE_NULL_AS_BLANK);
                    Object columnValueObject = null;
                    noOfValue++;
                    logger.debug("col: " + columnMap.get(noOfValue) +" type: " + currentCell.getCellType() + " value: ");
                    if (currentCell.getCellTypeEnum() == CellType.STRING) 
                    {
                    	String cellValue = currentCell.getStringCellValue();
                    	columnValueObject = cellValue;
                    	 if(!typeMap.containsKey(noOfValue))
                    	 {
                    		 typeMap.put(noOfValue, "VARCHAR");
                    		 sizeMap.put(noOfValue, cellValue.length());
                    	 }
                    	 else
                    	 {
                    		 
                    		 if(sizeMap.get(noOfValue) < cellValue.length())
                    			 sizeMap.put(noOfValue, cellValue.length());
                    	 }
                    } 
                    else if (currentCell.getCellTypeEnum() == CellType.NUMERIC)
                    {
                    	DataFormatter fmt = new DataFormatter();
                    	String cellValue = fmt.formatCellValue(currentCell);

                     	columnValueObject = cellValue;
                     	String type = "VARCHAR";
                     	if(Util.isParsableAsInt(cellValue))
                     		type = "INTEGER";
                     	else if(Util.isParsableAsLong(cellValue))
                     		type = "LONG";
                     	else if(Util.isParsableAsDouble(cellValue))
                     		type = "DOUBLE";
                     	
                     	 if(!typeMap.containsKey(noOfValue)){
                     		typeMap.put(noOfValue, type);
                     		if(type.equals("VARCHAR"))
                     			sizeMap.put(noOfValue, cellValue.length());
                     		else
                     			sizeMap.put(noOfValue, 0);
	                   	 }
	                   	 else
	                   	 {
	                   		
	                   		 if(typeMap.get(noOfValue).equals("VARCHAR") && sizeMap.get(noOfValue) < cellValue.length())
	                   			 sizeMap.put(noOfValue, cellValue.length());
	                   		 else if(typeMap.get(noOfValue).equals("INTEGER") && !type.equals("INTEGER"))
	                   			 typeMap.put(noOfValue, type);
	                   		 else if(typeMap.get(noOfValue).equals("LONG") && !type.equals("DOUBLE"))
	                   			typeMap.put(noOfValue, type);
	                   	 }
                    }
                    else if (currentCell.getCellTypeEnum() == CellType.BOOLEAN)
                    {
                    	String cellValue = currentCell.getBooleanCellValue()+"";

                    	columnValueObject = cellValue;
                    	if(!typeMap.containsKey(noOfValue))
                    	{
                   		 typeMap.put(noOfValue, "BOOLEAN");
                   		 sizeMap.put(noOfValue, cellValue.length());
	                   	 }
	                   	 else
	                   	 {
	                   		 if(sizeMap.get(noOfValue) < cellValue.length())
	                   			 sizeMap.put(noOfValue, cellValue.length());
	                   	 }
                    }
                    else if (currentCell.getCellTypeEnum() == CellType.FORMULA) 
                    {
                    	 String cellValue = currentCell.getCellFormula()+"";

                    	 columnValueObject = cellValue;
                    	 if(!typeMap.containsKey(noOfValue))
                    	 {
                    		 typeMap.put(noOfValue, "VARCHAR");
                    		 sizeMap.put(noOfValue, cellValue.length());
                    	 }
                    	 else
                    	 {
                    		 if(sizeMap.get(noOfValue) < cellValue.length())
                    			 sizeMap.put(noOfValue, cellValue.length());
                    	 }
                    }
                    else if (currentCell.getCellTypeEnum() == CellType.BLANK) 
                    {
                    	columnValueObject = null;
                    }
                    else if (currentCell.getCellTypeEnum() == CellType._NONE)
                    {
                    	columnValueObject = null;
                    }
                    else
                    {
                    	columnValueObject = null;
                    }
                    dataMap.put(noOfValue, columnValueObject);
                }
                dataList.add(dataMap);
               logger.debug(noOfValue + " added ");
           }
            fileData.setColumnMap(columnMap);
            fileData.setSizeMap(sizeMap);
            fileData.setTypeMap(typeMap);
            fileData.setDataList(dataList);
            return fileData;
        } 
		catch (FileNotFoundException e) 
		{
			logger.error("File Not Found Exception" + e.getMessage());
            e.printStackTrace();
        } 
		catch (IOException e) 
		{
			logger.error("IOException" + e.getMessage());
            e.printStackTrace();
        }
		finally
		{
			if(workbook != null)
			{
				logger.debug("close webhook");
				try
				{
					workbook.close();
				} 
				catch (IOException e) 
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
        return null;

    }
}
