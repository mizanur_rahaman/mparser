package com.placovuparser.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FileData {
	 String documentName;
	 Map<Integer,String> columnMap = new HashMap<Integer,String>();
     Map<Integer,String> typeMap = new HashMap<Integer,String>();
     Map<Integer,Integer> sizeMap = new HashMap<Integer,Integer>();
     ArrayList<Map<Integer,Object>> dataList = new ArrayList<Map<Integer,Object>>();
     
	public String getDocumentName() {
		return documentName;
	}
	public void setDocumentName(String excelSheetName) {
		this.documentName = excelSheetName;
	}
	public Map<Integer, String> getColumnMap() {
		return columnMap;
	}
	public void setColumnMap(Map<Integer, String> columnMap) {
		this.columnMap = columnMap;
	}
	public Map<Integer, String> getTypeMap() {
		return typeMap;
	}
	public void setTypeMap(Map<Integer, String> typeMap) {
		this.typeMap = typeMap;
	}
	public Map<Integer, Integer> getSizeMap() {
		return sizeMap;
	}
	public void setSizeMap(Map<Integer, Integer> sizeMap) {
		this.sizeMap = sizeMap;
	}
	public ArrayList<Map<Integer, Object>> getDataList() {
		return dataList;
	}
	public void setDataList(ArrayList<Map<Integer, Object>> dataList) {
		this.dataList = dataList;
	}
     
}
