package com.placovuparser.util;

public class Util {
	public static boolean isParsableAsInt(final String s) {
	    try {
	        Integer.valueOf(s);
	        return true;
	    } catch (NumberFormatException numberFormatException) {
	        return false;
	    }
	}
	public static boolean isParsableAsLong(final String s) {
	    try {
	        Long.valueOf(s);
	        return true;
	    } catch (NumberFormatException numberFormatException) {
	        return false;
	    }
	}

	public static boolean isParsableAsDouble(final String s) {
	    try {
	        Double.valueOf(s);
	        return true;
	    } catch (NumberFormatException numberFormatException) {
	        return false;
	    }
	}
}
