package com.placovuparser.manager.connection;

import org.springframework.context.annotation.Configuration;

@Configuration
public class JschRemoteConnection {
	
	private String hostName;
	private String userName; 
	private String password;
	private String strictHostKeyChecking;  
	private int port;
	private String remoteDirectory;
	private String localDirectory;
	private String fileType;
	private int totalField; 
	
	public String getHostName() {
		return hostName;
	}
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getStrictHostKeyChecking() {
		return strictHostKeyChecking;
	}
	public void setStrictHostKeyChecking(String strictHostKeyChecking) {
		this.strictHostKeyChecking = strictHostKeyChecking;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public String getRemoteDirectory() {
		return remoteDirectory;
	}
	public void setRemoteDirectory(String remoteDirectory) {
		this.remoteDirectory = remoteDirectory;
	}
	public String getLocalDirectory() {
		return localDirectory;
	}
	public void setLocalDirectory(String localDirectory) {
		this.localDirectory = localDirectory;
	}
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	public int getTotalField() {
		return totalField;
	}
	public void setTotalField(int totalField) {
		this.totalField = totalField;
	}
}
