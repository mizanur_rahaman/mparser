package com.placovuparser.manager.connection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.client.MongoDatabase;

@Repository
public class MongoDbConnectionManager {
	
	@Autowired MongoDbConnection mongoDbConnection;
	public MongoDatabase getMongoDbConnection()
	{
		  MongoClient mongo = new MongoClient( mongoDbConnection.getUrl() , Integer.parseInt(mongoDbConnection.getPort()) ); 
		  
		  MongoCredential credential = MongoCredential.createCredential(mongoDbConnection.getUsername(),
				  														mongoDbConnection.getDatabase(), 
				  														mongoDbConnection.getPassword().toCharArray()); 
	      System.out.println("Connected to the database successfully");  
	      // Accessing the database 
	      MongoDatabase database = mongo.getDatabase(mongoDbConnection.getDatabase());
	      return database;
	}
}
