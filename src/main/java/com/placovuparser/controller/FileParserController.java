package com.placovuparser.controller;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.placovuparser.service.TaskExecuterService;
import com.placovuparser.service.jsch.JschRemoteFetcher;


@RestController
public class FileParserController {

	@Autowired TaskExecuterService taskExecuterService;
	
	private static final Logger logger = Logger.getLogger(FileParserController.class);
	
	@RequestMapping(value="/parsefile",method=RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<String,String> parseFile(HttpServletRequest request,HttpServletResponse response) 
	{
		System.out.println("----------File parse ----------------");
		//taskExecuterService.InsertDataSqlServiceFromExcelFile();
		logger.debug("Downloading File From Remote...");
		taskExecuterService.downloadFileFromRemoteServerUsingJSCH();
         return null;
	}

}
