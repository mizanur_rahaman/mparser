package com.placovuparser.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController {
	@RequestMapping("/")
	public ModelAndView login() { 

		ModelAndView mv = new ModelAndView("home/index");
		return mv;
	}
}
