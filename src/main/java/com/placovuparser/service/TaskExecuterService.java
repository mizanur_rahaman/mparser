package com.placovuparser.service;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.placovuparser.manager.connection.JschRemoteConnection;
import com.placovuparser.parsermanager.ParserTaskScheduler;
import com.placovuparser.service.jsch.JschRemoteFetcher;

@Service
public class TaskExecuterService {
	
	private static final Logger logger = Logger.getLogger(ParserTaskScheduler.class);

	@Autowired FileParserService fileParserService;
	@Autowired JschRemoteFetcher jschRemoteFetcher;
	@Autowired JschRemoteConnection jschRemoteConnection;
   
	public void InsertDataSqlServiceFromExcelFile()
	{
		logger.info("execute InsertDataSqlServiceFromExcelFile");
		String filePath = jschRemoteConnection.getLocalDirectory();
		String fileType = jschRemoteConnection.getFileType();
		String files[] = new String[1000];
		int noOfReadFile = 0;
		try{
		    File dir = new File(filePath);
		    
			String[] children = dir.list();
			if (children == null) {
				logger.info("No file found to insert data");
			} 
			else 
			{
				for (int i = 0; i < children.length; i++)
				{
					String fileName = children[i];
					logger.info(fileName);
					if(fileName.endsWith("."+fileType))
					{	

						boolean success = fileParserService.parseExcelFile(filePath+"/"+fileName,jschRemoteConnection);
						files[noOfReadFile] = fileName;
						noOfReadFile++;
						if(success)
						{
							 System.out.println(fileName+" data inserted database successfully"); 
                            logger.info(fileName+" data inserted database successfully"); 
						}
						else{
							System.out.println(fileName+" data insertion failed");
							logger.info(fileName+" data insertion failed");
						}
					}
				}
				
				for (int i = 0; i < noOfReadFile; i++) 
				{
					//this.deleteFile(files[i]);
					logger.info("move file: "+files[i]);
					Files.move(Paths.get(filePath+"/"+files[i]), Paths.get(filePath+"/backup/"+files[i]), StandardCopyOption.REPLACE_EXISTING);
				}
			}

		}
		catch(Exception ex)
		{
			logger.error(ex.getMessage());
			ex.printStackTrace();
		}
		finally{}
	}
	
	public void downloadFileFromRemoteServerUsingJSCH()
	{
		logger.info("execute downloadFileFromRemoteServerUsingJSCH");
		try
		{
			jschRemoteFetcher.DownloadFileFromRemote(jschRemoteConnection);
			
		}
		catch(Exception e) 
		{
			logger.error(e.toString());
			e.printStackTrace();
		}
	}
}
