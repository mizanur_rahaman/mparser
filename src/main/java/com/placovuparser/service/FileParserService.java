package com.placovuparser.service;

import java.util.ArrayList;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import com.placovuparser.dao.FileParserDao;
import com.placovuparser.dao.RoboticSurgeryExtractWCostDao;
import com.placovuparser.manager.connection.JschRemoteConnection;
import com.placovuparser.model.FileData;
import com.placovuparser.parsermanager.ExcelFileParserManager;

@Service
public class FileParserService {

	private static final Logger logger = Logger.getLogger(FileParserService.class);

    @Autowired ExcelFileParserManager  excelFileParserManager;
    @Autowired RoboticSurgeryExtractWCostDao roboticSurgeryExtractWCostDao;
    @Autowired FileParserDao fileParserDao;
    @Autowired JdbcTemplate jdbcTemplate;
    
	public Boolean parseExcelFile(String filePath,JschRemoteConnection jschRemoteConnection) {
		logger.info("file name: "+filePath);
		Boolean success = false;
		fileParserDao.setJdbcTemplate(jdbcTemplate);
		FileData fileData = null;
		
		fileData = excelFileParserManager.processFile(filePath,jschRemoteConnection);
		
		logger.debug("file data: "+ fileData);
		if(fileData == null)
			return success;
		
		success = insertDataSqlServer(fileData,jschRemoteConnection);
		//insertDataMongoDbServer(excelFileParserManager.processFileForMongoDB(filePath));
		return success;
	}
	private void insertDataMongoDbServer (ArrayList<Document> dataList)
	{

		int size = dataList.size();

		for(int i=0;i<size;i++)
		{	
			roboticSurgeryExtractWCostDao.insertNewDocument(dataList.get(i), "robotic_surgery_extrack_wcost");
		}
		
	}
	private boolean insertDataSqlServer(FileData fileData,JschRemoteConnection jschRemoteConnection)
	{
		Boolean createTableSuccess =  true;
		Boolean insertDataSuccess = false;
		//createTableSuccess= this.createTable(fileData);
		//System.out.println("create table successfully: "+createTableSuccess);
		int insertedRowCount = 0;
		if(createTableSuccess)
		{
			insertDataSuccess = this.insertData(fileData,jschRemoteConnection);
		}
		return insertDataSuccess;
	}
	private Boolean createTable(FileData fileData,JschRemoteConnection jschRemoteConnection )
	{
		Map<Integer,Integer> sizeMap = fileData.getSizeMap();
		Map<Integer,String> typeMap = fileData.getTypeMap();
		Map<Integer,String> columnMap = fileData.getColumnMap();
		String commonField = "Id bigint not null identity(1,1), UniqueId varchar(128), PatientID varchar(50)";
				
		String columnString = "";
		int size = columnMap.size();
		for(int i=2;i<=size;i++)
		{
			if(typeMap.get(i) == null)
			{
				columnString += (i == 1? columnMap.get(i) + " varchar(50)" : ", "+ columnMap.get(i) + " varchar(50)");
				continue;
			}
			if(typeMap.get(i).equals("VARCHAR"))
				columnString += (i == 1? columnMap.get(i) + " varchar("+(sizeMap.get(i)+50)+")" : ", "+ columnMap.get(i) + " varchar("+(sizeMap.get(i)+50)+")");
			else if(typeMap.get(i).equals("DOUBLE"))
				columnString += (i == 1? columnMap.get(i) + " decimal(18,5)" : ", "+ columnMap.get(i) + " decimal(18,5)");
			else if(typeMap.get(i).equals("LONG"))
				columnString += (i == 1? columnMap.get(i) + " bigint" : ", "+ columnMap.get(i) + " bigint");
			else if(typeMap.get(i).equals("INTEGER"))
				columnString += (i == 1? columnMap.get(i) + " int" : ", "+ columnMap.get(i) + " int");
			else if(typeMap.get(i).equals("BOOLEAN"))
				columnString += (i == 1? columnMap.get(i) + " bit" : ", "+ columnMap.get(i) + " bit");
			else
				columnString += (i == 1? columnMap.get(i) + " varchar(50)" : ", "+ columnMap.get(i) + " varchar(50)");
			if(i==19)
			{	
				System.out.println("createTableColumnString-PatientSurgeryInformation: "+commonField+columnString);
				fileParserDao.createDatabaseTable("HospitalPatientSurgeryInformation", commonField+columnString); 
				columnString = "";
			}
			else if(i==26)
			{
				columnString = columnString.replace("1st", "First");
				columnString = columnString.replace("2nd","Second");
				System.out.println("createTableColumnString-PatientSurgeryProfessionalInformation: "+commonField+columnString);
				fileParserDao.createDatabaseTable("HospitalPatientSurgeryProfessionalInformation", commonField+columnString); 
				columnString = "";

			}
			else if(i==41)
			{
				System.out.println("createTableColumnString-PatientStatusInformation: "+commonField+columnString);
				fileParserDao.createDatabaseTable("HospitalPatientStatusInformation", commonField+columnString); 
				columnString = "";
			}
			else if(i==80)
			{
				System.out.println("createTableColumnString-PatientProcedureDetailInformation: "+commonField+columnString);
				fileParserDao.createDatabaseTable("HospitalPatientProcedureDetailInformation", commonField+columnString); 
				columnString = "";
			}
			else if(i==106)
			{
				System.out.println("createTableColumnString-HospitalPatientReadmissionInformation: "+commonField+columnString);
				fileParserDao.createDatabaseTable("HospitalPatientReadmissionInformation", commonField+columnString); 
				columnString = "";
			}
			else if(i==129)
			{
				System.out.println("createTableColumnString-HospitalPatientReadmissionServiceInformation: "+commonField+columnString);
				fileParserDao.createDatabaseTable("HospitalPatientReadmissionServiceInformation", commonField+columnString); 
				columnString = "";
			}
			else if(i==size)
			{
				System.out.println("createTableColumnString-HospitalPatientReadmissionDetailInformation: "+commonField+columnString);
				fileParserDao.createDatabaseTable("HospitalPatientReadmissionDetailInformation", commonField+columnString); 
				columnString = "";
			}


		}
		
		return true;
	}
	private boolean insertData(FileData fileData,JschRemoteConnection jschRemoteConnection)
	{

		try
		{
			Map<Integer,String> typeMap = fileData.getTypeMap();
			Map<Integer,String> columnMap = fileData.getColumnMap();
			ArrayList<Map<Integer,Object>> dataList = fileData.getDataList();
			String columnString = "";
			String valueString = "";
			int insertedRowCount = 0;
			int columnSize = columnMap.size();
			if(columnSize > jschRemoteConnection.getTotalField())
				columnSize = jschRemoteConnection.getTotalField();
			
			String commonColumnString = "UniqueId ";
			String commonValueString = "? ";
			logger.info("excel data row count: "+dataList.size());
			int dataSize = dataList.size();
			for(int i= 0;i<dataSize;i++)
			{
				Map<Integer,Object> dataMap= dataList.get(i);
			
				String uniqueID = UUID.randomUUID().toString();
				
				ArrayList<Object> params = new ArrayList<Object>();
				this.addCommonData(dataMap, params,uniqueID);
				
				for(int j= 1;j<=columnSize;j++)
				{
					columnString += " ,"+columnMap.get(j);
					valueString += " , ?";
					if(typeMap.get(j) != null)
					{
						if(typeMap.get(j).equals("VARCHAR"))
							params.add(dataMap.get(j) != null ? dataMap.get(j).toString() : null);
						else if(typeMap.get(j).equals("INTEGER"))
							params.add(dataMap.get(j) != null ? Integer.parseInt(dataMap.get(j).toString()) : 0);
						else if(typeMap.get(j).equals("LONG"))
							params.add(dataMap.get(j) != null ? Long.parseLong(dataMap.get(j).toString()) : 0);
						else if(typeMap.get(j).equals("DOUBLE"))
							params.add(dataMap.get(j) != null ? Double.parseDouble(dataMap.get(j).toString()) : 0);
						else if(typeMap.get(j).equals("BOOLEAN"))
							params.add(dataMap.get(j) != null ? Boolean.getBoolean(dataMap.get(j).toString()) : false);
						else
							params.add(dataMap.get(j) != null ? dataMap.get(j).toString() : null) ;
					}
					else
						params.add(dataMap.get(j) != null ? dataMap.get(j).toString() : null) ;

					if(j==19)
					{	
						logger.debug("PatientSurgeryInformation: ");
						logger.debug("insert column string: "+commonColumnString+columnString);
						logger.debug("insert value string: "+commonValueString+valueString);
						fileParserDao.insertRow("HospitalPatientSurgeryInformation", commonColumnString+columnString, commonValueString+valueString,params);
						columnString = "";
						valueString = "";
						params = new ArrayList<Object>();
			
						this.addCommonData(dataMap, params,uniqueID);
					}
					else if(j==26)
					{
						columnString = columnString.replace("1st", "First");
						columnString = columnString.replace("2nd","Second");
						logger.debug("PatientSurgeryProfessionalInformation: ");
						logger.debug("insert column string: "+commonColumnString+columnString);
						logger.debug("insert value string: "+commonValueString+valueString);
						fileParserDao.insertRow("HospitalPatientSurgeryProfessionalInformation", commonColumnString+columnString, commonValueString+valueString,params);
						columnString = "";
						valueString = "";
						params = new ArrayList<Object>();
						
						this.addCommonData(dataMap, params,uniqueID);

					}
					else if(j==41)
					{

						logger.debug("PatientStatusInformation: ");
						logger.debug("insert column string: "+commonColumnString+columnString);
						logger.debug("insert value string: "+commonValueString+valueString);
						fileParserDao.insertRow("HospitalPatientStatusInformation", commonColumnString+columnString, commonValueString+valueString,params);
						columnString = "";
						valueString = "";
						params = new ArrayList<Object>();
						this.addCommonData(dataMap, params,uniqueID);
					}
					else if(j==80)
					{
						logger.debug("PatientProcedureDetailInformation: ");
						logger.debug("insert column string: "+commonColumnString+columnString);
						logger.debug("insert value string: "+commonValueString+valueString);
						fileParserDao.insertRow("HospitalPatientProcedureDetailInformation", commonColumnString+columnString, commonValueString+valueString,params);
						columnString = "";
						valueString = "";
						params = new ArrayList<Object>();
						this.addCommonData(dataMap, params,uniqueID);
					}
					else if(j==106)
					{
						logger.debug("HospitalPatientReadmissionInformation: ");
						logger.debug("insert column string: "+commonColumnString+columnString);
						logger.debug("insert value string: "+commonValueString+valueString);
						fileParserDao.insertRow("HospitalPatientReadmissionInformation", commonColumnString+columnString, commonValueString+valueString,params);
						columnString = "";
						valueString = "";
						params = new ArrayList<Object>();
						this.addCommonData(dataMap, params,uniqueID);
					}
					else if(j==129)
					{
						logger.debug("HospitalPatientReadmissionServiceInformation: ");
						logger.debug("insert column string: "+commonColumnString+columnString);
						logger.debug("insert value string: "+commonValueString+valueString);
						fileParserDao.insertRow("HospitalPatientReadmissionServiceInformation", commonColumnString+columnString, commonValueString+valueString,params);
						columnString = "";
						valueString = "";
						params = new ArrayList<Object>();
						this.addCommonData(dataMap, params,uniqueID);
					}
					else if(j==columnSize)
					{
						logger.debug("HospitalPatientReadmissionDetailInformation: ");
						logger.debug("insert column string: "+commonColumnString+columnString);
						logger.debug("insert value string: "+commonValueString+valueString);
						fileParserDao.insertRow("HospitalPatientReadmissionDetailInformation", commonColumnString+columnString, commonValueString+valueString,params);
						columnString = "";
						valueString = "";
					}
				}
				insertedRowCount++;
			}
            logger.info("total row inserted: "+insertedRowCount);
		    return true;
		}
		catch(Exception ex)
		{
			logger.error(ex.getMessage());
			ex.printStackTrace();
		}
		return false;
		
	}
	
	public void addCommonData(Map<Integer,Object> dataMap,ArrayList<Object> params,String uniqueID ) throws Exception
	{
		params.add(uniqueID);
	}
}
