package com.placovuparser.service.jsch;

import java.util.Properties;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.placovuparser.manager.connection.JschRemoteConnection;

@Service
public class JschRemoteFetcher
{
	
	public static final String STRICT_HOST_KEY_CHECKING = "StrictHostKeyChecking";
	private static final Logger logger = Logger.getLogger(JschRemoteFetcher.class);
	public Session getJshSession(JschRemoteConnection jschRemoteConnection) {
		
		Properties propConfig = new Properties();
		propConfig.put(STRICT_HOST_KEY_CHECKING, jschRemoteConnection.getStrictHostKeyChecking());		
		Session session = null;
		JSch jsch = new JSch();
		
		try
		{
			session = jsch.getSession(jschRemoteConnection.getUserName(), jschRemoteConnection.getHostName(), jschRemoteConnection.getPort());			
		} 
		catch (JSchException e)
		{
			logger.error("Getting Session Failed: "+e);
			e.printStackTrace();
			throw new RuntimeException("Problem getting jsch session");
		}
		session.setPassword(jschRemoteConnection.getPassword());
		session.setConfig(propConfig);

		return session;
	}
	
	public void DownloadFileFromRemote(JschRemoteConnection jschRemoteConnection) 
	{
		Channel channel=null;
		Session session =null;
		
		try 
		{
			 session = this.getJshSession(jschRemoteConnection);
			 session.connect();
			 if(session.isConnected())
				 logger.debug("Session Connected");
			 
			 channel = session.openChannel("sftp");
			 logger.debug("Channel Opened");
			 
			 channel.connect(5000);
			 logger.debug("Channel Connected");
			
		     ChannelSftp sftp = (ChannelSftp) channel;
		     //sftp.connect();
		     logger.debug("SFTP Channel Connected");
		     
		     sftp.cd(jschRemoteConnection.getRemoteDirectory());
	         Vector filelist = sftp.ls(jschRemoteConnection.getRemoteDirectory());
	         
	         logger.info(filelist.size()+ " Files Found");
	         
	         int countFileDownloaded=0;
	         
	         for(int i=0; i<filelist.size();i++)
	         {
	        	 LsEntry entry = (LsEntry) filelist.get(i);
	             String fileName= entry.getFilename();
	             logger.info("Downloading File "+fileName);
	             System.out.println("Downloading File "+fileName);
	             logger.debug(jschRemoteConnection.getRemoteDirectory()+"/"+fileName+" Downloading...");
	             
	             if(fileName.endsWith("."+jschRemoteConnection.getFileType()) && !fileName.startsWith("AlreadyRead")) 
	             {
	            	 sftp.get(jschRemoteConnection.getRemoteDirectory()+"/"+fileName,jschRemoteConnection.getLocalDirectory());
	            	 
	            	 logger.debug(jschRemoteConnection.getRemoteDirectory()+"/"+fileName+" Download Completed");
	            	 
	            	 sftp.rename(fileName, "AlreadyRead"+fileName);
	            	 logger.debug(fileName+" Renamed");         	 
	            	 countFileDownloaded++;
	             }
	         }
	         
         	logger.info(countFileDownloaded+" Files Download Completed");
		}
	     catch (JSchException e) 
		{
	 	     logger.error(e);
	 	     e.printStackTrace();  
	 	}
	 	catch (SftpException e) 
		{
	 	    logger.error(e);
	 	    e.printStackTrace();
	 	}
		finally 
		{
			if(channel != null)
				channel.disconnect();
	     	logger.debug("Channel Disconnected on Finally");
	     	
	     	if(session != null)
	     		session.disconnect();
	     	logger.debug("Session Disconnected on Finally");
		
		}
	}
}
	
	

	
	

