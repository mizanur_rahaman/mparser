create database HospitalExtract
GO

use HospitalExtract
GO

CREATE TABLE [dbo].[HospitalPatientProcedureDetailInformation](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UniqueId] [varchar](128) NULL,
	[Procedure1Name] [varchar](200) NULL,
	[Procedure1Abbreviation] [varchar](200) NULL,
	[Procedure1Description] [varchar](500) NULL,
	[Procedure1ID] [varchar](200) NULL,
	[ProcedureLevel] [varchar](200) NULL,
	[Procedure2Name] [varchar](200) NULL,
	[Procedure2Abbreviation] [varchar](200) NULL,
	[Procedure2Description] [varchar](500) NULL,
	[Procedure3Name] [varchar](200) NULL,
	[Procedure3Abbreviation] [varchar](200) NULL,
	[Procedure3Description] [varchar](500) NULL,
	[TransferTo] [varchar](200) NULL,
	[DischargedTo] [varchar](200) NULL,
	[DischargeDate] [varchar](200) NULL,
	[WoundClass] [varchar](200) NULL,
	[EBL] [int] NULL,
	[PreOpDelayMinutes] [varchar](100) NULL,
	[PreOpDelayReasons] [varchar](200) NULL,
	[PreOpDelayComments] [varchar](500) NULL,
	[IntraOpDelayMinutes] [varchar](100) NULL,
	[IntraOpDelayReasons] [varchar](200) NULL,
	[IntraOpDelayComments] [varchar](500) NULL,
	[PostOpDelayMinutes] [varchar](100) NULL,
	[PostOpDelayReasons] [varchar](200) NULL,
	[PostOpDelayComments] [varchar](500) NULL,
	[ReferringProvider] [varchar](200) NULL,
	[ADTPatientClassAtDischarge] [varchar](200) NULL,
	[PointOfCancelation] [varchar](200) NULL,
	[CaseNotes] [varchar](500) NULL,
	[BMI] [decimal](18, 5) NULL,
	[ASA] [varchar](76) NULL,
	[TransfusionYN] [varchar](200) NULL,
	[RoomTurnoverRoomOutToInNBR] [decimal](18, 5) NULL,
	[RoomTurnoverPrecedingPrimaryProcedureNM] [varchar](200) NULL,
	[SurgeonTurnoverRoomOutToInNBR] [decimal](18, 5) NULL,
	[SurgeonTurnoverPrecedingPrimaryProcedureNM] [varchar](200) NULL,
	[AgeAtAdmit] [varchar](100) NULL,
	[TotalSupplyCost] [varchar](100) NULL,
	[TotalImplantCost] [varchar](100) NULL,
 CONSTRAINT [PK_HospitalPatientProcedureDetailInformation] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[HospitalPatientReadmissionDetailInformation](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UniqueId] [varchar](128) NULL,
	[ReAdmissionProcedure1Name] [varchar](200) NULL,
	[ReAdmissionProcedure1Abbreviation] [varchar](200) NULL,
	[ReAdmissionProcedure1Description] [varchar](500) NULL,
	[ReAdmissionProcedure1ID] [varchar](200) NULL,
	[ReAdmissionProcedureLevel] [varchar](200) NULL,
	[ReAdmissionProcedure2Name] [varchar](200) NULL,
	[ReAdmissionProcedure2Abbreviation] [varchar](200) NULL,
	[ReAdmissionProcedure2Description] [varchar](500) NULL,
	[ReAdmissionProcedure3Name] [varchar](200) NULL,
	[ReAdmissionProcedure3Abbreviation] [varchar](200) NULL,
	[ReAdmissionProcedure3Description] [varchar](500) NULL,
	[ReAdmissionTransferTo] [varchar](200) NULL,
	[ReAdmissionDischargedTo] [varchar](200) NULL,
	[ReAdmissionDischargeDate] [varchar](200) NULL,
	[ReAdmissionWoundClass] [varchar](200) NULL,
	[ReAdmissionEBL] [int] NULL,
	[ReAdmissionPreOpDelayMinutes] [varchar](100) NULL,
	[ReAdmissionPreOpDelayReasons] [varchar](200) NULL,
	[ReAdmissionPreOpDelayComments] [varchar](500) NULL,
	[ReAdmissionIntraOpDelayMinutes] [varchar](100) NULL,
	[ReAdmissionIntraOpDelayReasons] [varchar](200) NULL,
	[ReAdmissionIntraOpDelayComments] [varchar](500) NULL,
	[ReAdmissionPostOpDelayMinutes] [varchar](100) NULL,
	[ReAdmissionPostOpDelayReasons] [varchar](200) NULL,
	[ReAdmissionPostOpDelayComments] [varchar](500) NULL,
	[ReAdmissionReferringProvider] [varchar](200) NULL,
	[ReAdmissionADTPatientClassAtDischarge] [varchar](200) NULL,
	[ReAdmissionPointOfCancelation] [varchar](200) NULL,
	[ReAdmissionCaseNotes] [varchar](500) NULL,
	[ReAdmissionBMI] [int] NULL,
	[ReAdmissionASA] [varchar](200) NULL,
	[ReAdmissionTransfusionYN] [varchar](200) NULL,
	[ReAdmissionAgeAtAdmit] [varchar](200) NULL,
 CONSTRAINT [PK_HospitalPatientReadmissionDetailInformation] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[HospitalPatientReadmissionInformation](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UniqueId] [varchar](128) NULL,
	[ReAdmissionDepartmentName] [varchar](200) NULL,
	[ReAdmissionLOS] [int] NULL,
	[ReAdmissionAdmitDate] [varchar](100) NULL,
	[ReAdmissionDischargeDate] [varchar](100) NULL,
	[ReAdmissionDischargeDisposition] [varchar](200) NULL,
	[ReAdmissionPrimaryICDProcedure] [varchar](200) NULL,
	[ReAdmissionPrimaryICDDiagnosis] [varchar](200) NULL,
	[DaysBetweenSurgeryAndReAdmission] [int] NULL,
	[ReAdmissionMSDRG] [varchar](200) NULL,
	[ReAdmissionSurgicalLocation] [varchar](200) NULL,
	[ReAdmissionSurgeryDate] [varchar](200) NULL,
	[ReAdmissionSurgeryDOW] [varchar](200) NULL,
	[ReAdmissionScheduledInTime] [varchar](200) NULL,
	[ReAdmissionPreOpStart] [varchar](200) NULL,
	[ReAdmissionORReady] [varchar](200) NULL,
	[ReAdmissionInOR] [varchar](200) NULL,
	[ReAdmissionIncision] [varchar](200) NULL,
	[ReAdmissionRobotStart] [varchar](200) NULL,
	[ReAdmissionRobotEnd] [varchar](200) NULL,
	[ReAdmissionClosingStart] [varchar](200) NULL,
	[ReAdmissionDressingOn] [varchar](200) NULL,
	[ReAdmissionOutOR] [varchar](200) NULL,
	[ReAdmissionORMinutes] [int] NULL,
	[ReAdmissionORScheduleMinutes] [int] NULL,
	[ReAdmissionAnesthesiaStart] [varchar](200) NULL,
	[ReAdmissionAnesthesiaEnd] [varchar](200) NULL,
 CONSTRAINT [PK_HospitalPatientReadmissionInformation] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[HospitalPatientReadmissionServiceInformation](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UniqueId] [varchar](128) NULL,
	[ReAdmissionSurgicalService] [varchar](200) NULL,
	[ReAdmissionPrimarySurgeonName] [varchar](200) NULL,
	[ReAdmissionPrimarySurgeonID] [varchar](200) NULL,
	[ReAdmissionPrimarySurgeonGroup1] [varchar](200) NULL,
	[ReAdmissionPrimarySurgeonGroup2] [varchar](200) NULL,
	[ReAdmissionPrimarySurgeonGroup3] [varchar](200) NULL,
	[ReAdmission2ndSurgeonNamePanel1] [varchar](200) NULL,
	[ReAdmission1stSurgeonNamePanel2] [varchar](200) NULL,
	[ReAdmissionPatientClass] [varchar](200) NULL,
	[ReAdmissionCaseClass] [varchar](200) NULL,
	[ReAdmissionAnesthesiaType] [varchar](200) NULL,
	[ReAdmissionMDAID] [varchar](200) NULL,
	[ReAdmissionMDAName] [varchar](200) NULL,
	[ReAdmissionCRNAID] [varchar](200) NULL,
	[ReAdmissionCRNAName] [varchar](200) NULL,
	[ReAdmissionCirculatorID] [varchar](200) NULL,
	[ReAdmissionCirculatorName] [varchar](200) NULL,
	[ReAdmissionCirculatorStartTime] [varchar](200) NULL,
	[ReAdmissionCirculatorEndTime] [varchar](200) NULL,
	[ReAdmissionScrubID] [varchar](200) NULL,
	[ReAdmissionScrubName] [varchar](200) NULL,
	[ReAdmissionScrubStartTime] [varchar](200) NULL,
	[ReAdmissionScrubEndTime] [varchar](200) NULL,
 CONSTRAINT [PK_HospitalPatientReadmissionServiceInformation] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[HospitalPatientStatusInformation](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UniqueId] [varchar](128) NULL,
	[PatientClass] [varchar](200) NULL,
	[CaseClass] [varchar](200) NULL,
	[AnesthesiaType] [varchar](200) NULL,
	[MDAID] [varchar](200) NULL,
	[MDAName] [varchar](200) NULL,
	[CRNAID] [varchar](200) NULL,
	[CRNAName] [varchar](200) NULL,
	[CirculatorID] [varchar](200) NULL,
	[CirculatorName] [varchar](200) NULL,
	[CirculatorStartTime] [varchar](200) NULL,
	[CirculatorEndTime] [varchar](200) NULL,
	[ScrubID] [varchar](200) NULL,
	[ScrubName] [varchar](200) NULL,
	[ScrubStartTime] [varchar](200) NULL,
	[ScrubEndTime] [varchar](200) NULL,
 CONSTRAINT [PK_HospitalPatientStatusInformation] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[HospitalPatientSurgeryInformation](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UniqueId] [varchar](128) NULL,
	[PatientID] [varchar](200) NULL,
	[SurgicalLocationName] [varchar](200) NULL,
	[SurgeryDate] [varchar](200) NULL,
	[DayOfWeek] [varchar](200) NULL,
	[ScheduledInTime] [varchar](200) NULL,
	[PreOpStart] [varchar](200) NULL,
	[ORReady] [varchar](200) NULL,
	[InOR] [varchar](200) NULL,
	[Incision] [varchar](200) NULL,
	[RobotStart] [varchar](200) NULL,
	[RobotEnd] [varchar](200) NULL,
	[ClosingStart] [varchar](200) NULL,
	[DressingOn] [varchar](200) NULL,
	[OutOR] [varchar](200) NULL,
	[ORMinutes] [int] NULL,
	[ORScheduleMinutes] [int] NULL,
	[AnesthesiaStart] [varchar](200) NULL,
	[AnesthesiaEnd] [varchar](200) NULL,
	[SurgicalService] [varchar](200) NULL,
 CONSTRAINT [PK_HospitalPatientSurgeryInformation] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[HospitalPatientSurgeryProfessionalInformation](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UniqueId] [varchar](128) NULL,
	[PrimarySurgeonName] [varchar](200) NULL,
	[PrimarySurgeonID] [varchar](200) NULL,
	[PrimarySurgeonGroup1] [varchar](200) NULL,
	[PrimarySurgeonGroup2] [varchar](200) NULL,
	[PrimarySurgeonGroup3] [varchar](200) NULL,
	[SecondSurgeonNamePanel1] [varchar](200) NULL,
	[FirstSurgeonNamePanel2] [varchar](200) NULL,
 CONSTRAINT [PK_HospitalPatientSurgeryProfessionalInformation] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
